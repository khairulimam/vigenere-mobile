package com.keren.mantap.vigenere;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnJosh, btnRandomize;
    private EditText etText;
    private RadioGroup rGroup;
    private TextView processedText, etWarn;
    private ScrollView svTextArea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setSubtitle("C2L Josh");
        btnJosh = (Button) findViewById(R.id.btnJosh);
        btnRandomize = (Button) findViewById(R.id.btnRandom);
        etText = (EditText) findViewById(R.id.etText);
        rGroup = (RadioGroup) findViewById(R.id.rgEncryptMethod);
        processedText = (TextView) findViewById(R.id.tvProcessedText);
        etWarn = (TextView) findViewById(R.id.etWarn);
        svTextArea = (ScrollView) findViewById(R.id.svTextArea);
        btnJosh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = etText.getText().toString();
                if (text.trim().length() <= 0) return;
                int selectedMethod = rGroup.getCheckedRadioButtonId();
                if (selectedMethod != R.id.rbEncrypt && selectedMethod != R.id.rbDecrypt) {
                    Toast.makeText(getApplicationContext(), "Sabar! Pilih method dulu!", Toast.LENGTH_LONG).show();
                    return;
                }
                switch (selectedMethod) {
                    case R.id.rbEncrypt:
                        new ManipulateText().execute(text, Manipulate.ENCRYPT);
                        break;
                    case R.id.rbDecrypt:
                        new ManipulateText().execute(text, Manipulate.DECRYPT);
                        break;
                }
            }
        });
        btnRandomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = etText.getText().toString();
                if (text.trim().length() <= 0) return;
                new ManipulateText().execute(text, Manipulate.RANDOMIZE);
            }
        });
//        copy text to clip board
        processedText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d("imamJosh", "Scroll View long clicked!");
                ClipboardManager clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                String data = processedText.getText().toString();

                if (data.trim().length() <= 0) {
                    Toast.makeText(getApplicationContext(), "Sorry... no data to be copied! :(", Toast.LENGTH_LONG).show();
                    return true;
                }
                ClipData clipText = ClipData.newPlainText("josh", data);
                clipBoard.setPrimaryClip(clipText);
                Toast.makeText(getApplicationContext(), "Tinggal dipaste broh!", Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionSetting:
                showOverrideDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showOverrideDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Override Default Key");
        final View overrideKeyDialog = getLayoutInflater().inflate(R.layout.override_key_dialog, null);
        dialog.setView(overrideKeyDialog);
        final EditText etNewKey = (EditText) overrideKeyDialog.findViewById(R.id.etNewKey);
        dialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String newKey = new String(etNewKey.getText().toString());
                if (newKey.trim().length() <= 0) {
                    Toast.makeText(getApplicationContext(), "Nothing is edited", Toast.LENGTH_LONG).show();
                    return;
                }
                SharedPreferences.Editor keyEditor = getSharedPreferences(getString(R.string.editor_key), MODE_PRIVATE).edit();
                keyEditor.putString(getString(R.string.key_key), newKey);
                keyEditor.apply();
                Toast.makeText(getApplicationContext(), "Saved " + newKey + " as a new key!", Toast.LENGTH_LONG).show();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class ManipulateText extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Manipulate vigenere = new Manipulate(params[0], params[1], getApplicationContext());
            return vigenere.getText();
        }

        @Override
        protected void onPostExecute(String s) {
            processedText.setText(s);

        }
    }
}
