package com.keren.mantap.vigenere;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by khairulimam on 24/04/16.
 */
public class Manipulate {
    private String text;
    private Context context;
    private static final int ROUND_MODULUS = 26;

    public static final String ENCRYPT = "ENCRYPT";
    public static final String DECRYPT = "DECRYPT";
    public static final String RANDOMIZE = "RANDOMIZE";

    public Manipulate(String text, String flag, Context context) {
        this.context = context;
        this.text = text;
        switch (flag) {
            case ENCRYPT:
                this.text = text.toLowerCase().replaceAll("\\s","");
                this.encrypt();
                break;
            case DECRYPT:
                this.text = text.toLowerCase().replaceAll("\\s","");
                this.decrypt();
                break;
            case RANDOMIZE:
                randomize();
                break;
        }
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text.toLowerCase().replaceAll("\\s", "");
    }

    public void encrypt() {
        String tmp = "";
        for (int i = 0; i < this.text.length(); i++) {
            tmp += (char) ((((text.charAt(i) - 97) + (getKey().charAt(i) - 97)) % ROUND_MODULUS) + 97);
        }
        this.text = tmp;
    }

    public void decrypt() {
        String tmp = "";
        for (int i = 0; i < this.text.length(); i++) {
            int ti = text.charAt(i) - 97;
            int ki = getKey().charAt(i) - 97;
            tmp += (char) (modulo(ti-ki, ROUND_MODULUS) + 97);
        }
        this.text = tmp;
    }

    private int modulo(int a, int b) {
        int r = a % b;
        return (r < 0) ? r + b : r;
    }

    private String getKey() {
        String influencedKey = "";
        SharedPreferences savedKey = context.getSharedPreferences(context.getString(R.string.editor_key), context.MODE_PRIVATE);
        String key = savedKey.getString(context.getString(R.string.key_key), "mantapjosh").toLowerCase().replaceAll("\\s","");
        int index = 0;
        for (int i = 0; i < this.text.length(); i++) {
            index = i;
            if (index > key.length()-1)
                index = 0;
            influencedKey += key.charAt(index);
        }
        return influencedKey;
    }

    private void randomize() {
        String[] j = text.split(" ");
        String tmp = "";
        for(String t:j) {
            tmp += alay(t)+" ";
        }
        this.text = tmp;
    }

    private static String alay(String word) {
        String tmp = word;
        int wordLength = word.length();
        ArrayList<Integer> indexes = new ArrayList<>();
        for(int i=1; i<wordLength-1; i++) {
            indexes.add(i);
        }
        //randomize the value of indexes
        Collections.shuffle(indexes);
        char[] tmpChars = word.toCharArray();
        for(int i=1; i<wordLength-1;i++) {
            tmpChars[i] = tmp.charAt(indexes.get(i-1));
        }
        return new String(tmpChars);
    }

}
